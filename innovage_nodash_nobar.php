<?php

/**
 * Plugin Name: Innovage NoDash NoBar
 * Plugin URI: https://bitbucket.org/SinOB/innovage_nodash_nobar
 * Description: Hide the admin bar, dashboard and user profile for all users except admin
 * Version: 1.0
 * Author: Sinead O'Brien
 * Author URI: https://bitbucket.org/SinOB
 * Requires at least: 3.8.1
 * Tested up to: 4.0.1
 * License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

// Load options instance
if (class_exists('Innovage_NoDashNoBar')) {
    $load = new Innovage_NoDashNoBar;
}

class Innovage_NoDashNoBar {
    /*
     * InnovAge NoDashNoBar init
     */

    function __construct() {
        $this->hooks();
    }

    /**
     * Action Hooks and Filters
     */
    function hooks() {

        // Hide the admin bar for all users except admin
        add_filter('show_admin_bar', array($this, 'hide_admin_bar'));
        // Hide the dashboard and profile for all users except admin
        add_action('admin_init', array($this, 'hide_dashboard'));
    }

    /*
     * Admin bar hide
     * 
     */

    function hide_admin_bar($content) {
        return ( current_user_can('administrator') ) ? $content : false;
    }

    /**
     * Dashboard and user profile redirect
     * 
     * @global type $pagenow
     */
    function hide_dashboard() {
        global $pagenow;

        if (!current_user_can('administrator')) {
            if ('profile.php' == $pagenow) {
                wp_redirect(home_url());
                exit;
            }
        }
    }

}
