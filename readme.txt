=== Innovage_NoDashNoBar ===
Contributors: SinOB
Requires at least: 3.8.1
Tested up to: 4.0.1
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Hide the admin bar, dashboard and user profile for all users except admin for iStep.



== Description ==

Innovage NoDashNoBar is a plugin to hide the following sections from all users 
except the administrator;
1) the dashboard
2) the admin bar
3) the user profile
If a user attempts to go to the profile page or dashboard directly they will be 
automatically redirected back to the sites home page.

There are currently no admin options.

Not suitable for multisite wordpress install (untested) 

There are existing plugins that will provide SOME of this functionality already
but it appears would have to install multiple plugins to get these and would end
up with unnecessary extras.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
